import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Main {
    private static ArrayList<String> szavak = new ArrayList<>();
    private static Scanner s = new Scanner(System.in);
    private static String gondoltSzo = "";
    private static int counter;
    private static char[] gondoltSzoBetui;

    private static void feltoltes() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("src/otbetus.txt"));
        while (sc.hasNext()) {
            szavak.add(sc.next());
        }
        Random r = new Random();
        int ridx = r.nextInt(szavak.size());
        gondoltSzo = szavak.get(ridx);
        gondoltSzoBetui = gondoltSzo.toCharArray();
        sc.close();
    }

    private static void tipp() {
        counter = 0;
        System.out.println("Mi az 5 betűs szavad?");
        String tipp = s.next();
        char[] betuk = tipp.toCharArray();
        for (int i = 0; i < 5; i++) {
            if (betuk[i] == gondoltSzoBetui[i]) {
                counter++;
            }
        }
        System.out.println(gondoltSzo);
        System.out.println(counter + ", egyezést találtam a gondolt szó és az általad írt szó között");
    }

    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("\nÜdvözöllek az ötbetűs kitalálós játékban!\n");
        System.out.println("Kigondoltam egy 5 betűs szót, kérlek találd ki.");
        feltoltes();
        do {
            tipp();
        } while (counter != 5);
        System.out.println("Gratulálok, ez a gondolt szó");
    }
}
